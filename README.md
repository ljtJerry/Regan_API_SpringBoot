<div align="center">
    <h1>Regan_API_SpringBoot</h1>
</div>
<p align="center">
<a href='https://gitee.com/regan_org/Regan_API_SpringBoot/stargazers'><img src='https://gitee.com/regan_org/Regan_API_SpringBoot/badge/star.svg?theme=dark' alt='star'></img></a>
<a href='https://gitee.com/regan_org/Regan_API_SpringBoot/members'><img src='https://gitee.com/regan_org/Regan_API_SpringBoot/badge/fork.svg?theme=dark' alt='fork'></img></a>
</p>

#### 项目介绍
Regan_API_SpringBoot 文档项目
***Regan API*** 项目是基于注释自动生成api文档，很大缩短了开始与后期维护API接口文档的时间。***Regan API*** 利用jdk提供的Doclet
类读取文档注释，可手动配置需要读取的文件，同时增加了读取过滤指定方法过滤等功能。

![](http://file.homeins.cn/FjnP0FvBDFwKRH4LLFwzYyI_tvbH)

![](http://file.homeins.cn/FrIAtiOVuYau1WLQ33M3w4Sqj4q5)

#### java源码
``` java
 /**
 * 用户管理
 * @action /user
 * @author zhangby
 * @date 2018/6/12 下午3:26
 */
public class UserController extends Controller {
    
    /**
     * 用户登录功
     * @title 登录接口
     * @param username|用户名|string|必填
     * @param password|密码|string|必填
     * @resqParam code|用户名|String|必填
     * @resqParam data|数据|object|非必填
     * @resqParam msg|消息信息|String|必填
     * @respBody {"code":"000","data":"","msg":"success"}
     * @requestType post,get
     * @author zhangby
     * @date 2018/6/12 下午4:23
     */
    public void login() {
        renderJson(Kv.create().set("code","000"));
    }

}

```

#### 文档： [https://zhangbiy.github.io/regan_api_doc/#/](https://zhangbiy.github.io/regan_api_doc/#/)

#### 版本
- Jfinal: [https://gitee.com/regan_jeff/jfinal-api](https://gitee.com/regan_jeff/jfinal-api)
- springboot: [https://gitee.com/regan_jeff/Regan_API_SpringBoot](https://gitee.com/regan_jeff/Regan_API_SpringBoot)
#### 软件架构
软件架构说明
***Regan API*** 基础项目基于 springboot 开发，前端基于[飞冰](https://alibaba.github.io/ice)开发的API接口文档管理项目。

#### 欢迎加入 [regan_org组织【编程爱好者】](https://gitee.com/organizations/regan_org/invite?invite=dddf200261b5b5b3cc442eb5d661b872ad4de3598d087d4c00dff9da30dd446d17494c4c2357cf5d)